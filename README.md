# pipe-it-down


## Extremely barebones implementation of pipes, inspired by Elixir.

To use it, simply kick off a chain, optionally supplying an initial value.

If no initial value, it will default to null.

#### Example:

Javascript:

```javascript

 // node import
 const { chain } = require('pipe-it-down');
 
 // es6-style import
 import { chain } from 'pipe-it-down';

 const totalDataset = chain(dataset)
   .pipe( normalize )
   .pipe( assignWeights(weights) )
   .pipe( average )
   .end();
```

Which would be equivalent to typing out:

```javascript
const totalDataset = average(assignWeights(weights)(normalize(dataset)))
```

or somewhat cleaner:

```javascript
const normalized = normalize(dataset);
const weighted = assignWeights(weights)(normalized);
const totalDataset = average(weighted);
```

#### Note:

If operating over arrays, this is very similar to map/filter/reduce combinations.
