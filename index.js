/**
 *  Extremely barebones implementation of pipes, inspired by Elixir.
 *
 *  To use it, simply kick off a chain, optionally supplying an initial value.
 *  If no initial value, it will default to null.
 *
 *  @example:
 *
 *    const totalDataset = chain(dataset)
 *      .pipe( normalize )
 *      .pipe( assignWeights(weights) )
 *      .pipe( average )
 *      .end();
 *
 *  Elixir-equivalent:
 *  totalDataset = dataset
 *    |> normalize
 *    |> average

 *  Which would be equivalent to typing out:
 *    const totalDataset = average(assignWeights(weights)(normalize(dataset)))
 *  or:
 *    const normalized = normalize(dataset);
 *    const weighted = assignWeights(weights)(normalized);
 *    const totalDataset = average(weighted);
 *
 *
 *  @note: if operating over arrays, this is very similar to map/filter/reduce
 *         combinations.
 *
 */

module.exports = {chain};

function chain(initialVal) {
  let arg = initialVal || null;

  return { pipe, end };

  function pipe(fn) {
    const val = fn(arg);
    // update
    arg = val;
    return { pipe, end };
  }

  function end() {
    return arg;
  }
}
