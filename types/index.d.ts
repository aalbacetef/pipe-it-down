
declare module "pipe-it-down" {
  export function end(): any;
  export function pipe(fn: Function): object;
  export function chain(initialVal: any): { pipe: typeof pipe, end: typeof end };
}
